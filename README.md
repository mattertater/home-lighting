# Home Lighting

Home automation code controlling the RGB LED strips on my main floor. Patterns, controls, etc. Running on Raspberry Pi Zero W

## Ideas

* Read from Pi's environment variables to choose what 'mode' to be in (reactive, party, normal, etc).
* Separate out the more complex algorithms into individual .py files as good practice
* Environment wide variables for colors like warm white, etc?
* Use flask to run the webserver to hold the API endpoints?