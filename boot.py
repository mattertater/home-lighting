import time, os
from rpi_ws281x import *

LED_COUNT      = 900      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 10     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

def allOff(strip):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, 0)
    strip.show()

def allOn(strip, color):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, color)
    strip.show()

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

# Blink twice on boot showing start of master light control
allOn(strip, Color(255, 255, 255))
time.sleep(0.025)
allOff(strip)
time.sleep(0.1)

allOn(strip, Color(255, 255, 255))
time.sleep(0.025)
allOff(strip)
time.sleep(0.1)