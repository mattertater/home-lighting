#!/usr/bin/env python3

# TODO: implement flags or checks to see if the mode has changed 

import time, os, random
from rpi_ws281x import *
from dotenv import load_dotenv

class Values:
    strip1 = range(0, 150)
    strip2 = range(150, 300)[::-1]
    strip3 = range(300, 450)
    strip4 = range(450, 600)[::-1]
    strip5 = range(600, 750)
    strip6 = range(750, 900)[::-1]
    strips = [strip1, strip2, strip3, strip4, strip5, strip6]

    colors = {  "WARM_WHITE" : Color(255, 197, 143),
                "COOL_WHITE" : Color(212, 235, 255),
                "WHITE" : Color(255, 255, 255),
                "RED" : Color(255, 0, 0),
                "GREEN" : Color(0, 255, 0),
                "BLUE" : Color(0, 0, 255),
                "GROW_LIGHT" : Color(167, 0, 255) }

    # LED strip configuration:
    LED_COUNT      = 900      # Number of LED pixels.
    LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
    LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
    LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
    LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


class Functions:

    # Define functions which animate LEDs in various ways.
    def setStrip(self, strip, color):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
        strip.show()

    def colorWipe(self, strip, color, wait_ms=50, speed=2):
        """Wipe color across display a pixel at a time."""
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
            if (i % (speed*2)) == 0:
                strip.show()

    def wheel(self, pos):
        """Generate rainbow colors across 0-255 positions."""
        if pos < 85:
            return Color(pos * 3, 255 - pos * 3, 0)
        elif pos < 170:
            pos -= 85
            return Color(255 - pos * 3, 0, pos * 3)
        else:
            pos -= 170
            return Color(0, pos * 3, 255 - pos * 3)

    def rainbow(self, strip, wait_ms=20, iterations=1, speed=2):
        """Draw rainbow that fades across all pixels at once."""
        for j in range(256*iterations):
            for i in range(strip.numPixels()):
                strip.setPixelColor(i, wheel((i+j) % 255))
            if (j%speed) == 0:
                strip.show()

    def rainbowCycle(self, strip, iterations=5):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        for j in range(256*iterations):
            for i in range(strip.numPixels()):
                strip.setPixelColor(i, self.wheel((int(i * 256 / strip.numPixels()) + j) & 255))
            strip.show()

    def allOff(self, strip):
        print('Turning everything off')
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, 0)
        strip.show()

    def fadeOn(self, strip, color, speed=2, max=255):
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, color)
        for i in range(0, max, speed):
            strip.setBrightness(i)
            strip.show()
            time.sleep(0.001)

    def fadeOff(self, strip, color, speed=2, max=255):
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, color)
        for i in range(max, 0, -speed):
            strip.setBrightness(i)
            strip.show()
            time.sleep(0.001)

    def stripOn(self, strip, color, speed=1):
        for i in range(0, 150):
            for s in strips:
                strip.setPixelColor(s[i], color)
            if (i % speed) == 0:
                strip.show()
        strip.show()

    def upOrDown(self, f, t):
        if f - t < 0:
            return 1
        return -1

    def getDelta(self, f, t, s):
        return float(abs(f - t) / s) * self.upOrDown(f, t)

    def rainbowFade(self, strip, toR, toG, toB, speed=1):
        steps = 20 - speed
        if steps < 1:
            steps = 1
        # Get current color
        currentColor = strip.getPixelColorRGB(0)
        fromR = currentColor.r
        fromG = currentColor.g
        fromB = currentColor.b
        rDelta = self.getDelta(fromR, toR, steps)
        gDelta = self.getDelta(fromG, toG, steps)
        bDelta = self.getDelta(fromB, toB, steps)

        for j in range(0, steps):
            newR = fromR + rDelta * j
            newG = fromG + gDelta * j
            newB = fromB + bDelta * j
            newColor = Color(int(round(newR)), int(round(newG)), int(round(newB)))
            for i in range(0, strip.numPixels()):
                strip.setPixelColor(i, newColor)

            strip.show()

    def stripTest(self, strip, stripNum):
        for i in range(0, strip.numPixels()):
            if i in strips[stripNum]:
                strip.setPixelColor(i, Color(255, 255, 255))
                strip.show()

    def stripColor(self, strip, color):
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, color)
        strip.show()


# try:
#     while True:

#         loadenv(env)

#         if env['MODE'] == 'SOLID':
#             stripColor(strip, Color(env['R'], env['G'], env['B']))

#         elif env['MODE'] == 'RAINBOW':
#             rainbowFade(strip, random.randrange(0,255), random.randrange(0,255), random.randrange(0,255), random.randrange(1,10))

#         else:
#             stripColor(strip, colors["WARM_WHITE"])


# except KeyboardInterrupt:
#     allOff(strip)
