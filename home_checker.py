import subprocess
import requests
from time import sleep

# My phones MAC address
address = "c0:ee:fb:d4:c8:5f"


# Give pi time to connect to network on boot
sleep(30)

count = 0

while True:
    sleep(5)
    output = subprocess.check_output("sudo arp-scan -l", shell=True)
    print(output)
    if address in output:
        count = 0
        print('connected, turning lights on')
        requests.post('http://localhost:6969', data = {'id':'picker', 'colorpicker':'rgb(120,90,40)', 'auto':'True'})
        # Probably going to be connected for a while, wait 15 minutes to check again
        sleep(900)
    else:
        count = count + 1
        print('not connected: {}'.format(count)) 
        # check if disconnected for 10 checks in a row
        if count > 10:
            count = 0
            print('disconnecting lights, waiting 5 minutes')
            requests.post('http://localhost:6969', data = {'id':'special', 'routine':'turn_off', 'auto':'True'})
            sleep(300)

