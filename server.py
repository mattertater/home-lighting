from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from flask_colorpicker import colorpicker
import os, time, random
from dotenv import load_dotenv

from rpi_ws281x import *

from controller import Values, Functions
import numpy as np
import threading

# Initialize flask app, plug in Bootstrap and colorpicker
app = Flask(__name__, template_folder='.')
Bootstrap(app)
colorpicker(app)
load_dotenv(override=True)

# Make flag to check what mode we're in, jsut default it to solid. 
# Right now we only really care about it being rainbow_fade, but thatll change
flag = 'solid'

# Make classes to access the separate functions and constants we need
vals = Values()
func = Functions()

# Create and initialize the LED strip, clearing all the LEDs too
strip = Adafruit_NeoPixel(vals.LED_COUNT, vals.LED_PIN, vals.LED_FREQ_HZ, vals.LED_DMA, vals.LED_INVERT, vals.LED_BRIGHTNESS, vals.LED_CHANNEL)
strip.begin()
func.allOff(strip)

# Load current strip state
mode = os.getenv("MODE")
if mode == "SOLID":
    solid_checked="checked"
    rainbow_checked=""
else:
    solid_checked=""
    rainbow_checked="checked"

r = int(os.getenv("R"))
g = int(os.getenv("G"))
b = int(os.getenv("B"))

# Flag if i want the auto light turn on or off script to run
global allow_auto, allow_auto_checked
allow_auto = True
allow_auto_checked = "checked"
os.environ['ALLOW_AUTO'] = str(allow_auto)
os.environ['ALLOW_AUTO_CHECKED'] = allow_auto_checked

def setenv(key, val):
    os.system('dotenv --quote never set {} {}'.format(key, val))

def lightingHelper(strip):
    while True:
        print('rainbow fade!!')
        if flag == 'rainbow_fade':
            rv = random.randint(0, 100)
            gv = random.randint(0, 100)
            bv = random.randint(0, 100)
            speed = random.randint(1, 10)
            func.rainbowFade(strip, rv, gv, bv, speed)
        else:
            time.sleep(0.1)

# Start the lighting helper thread
# lightingHelperThread = threading.Thread(target = lightingHelper, args = (strip,))
# lightingHelperThread.start()


def get_allow_auto():
    return allow_auto

def get_allow_auto_checked():
    return allow_auto_checked

def set_allow_auto(val):
    allow_auto = val
    if val:
        allow_auto_checked = "checked"
    else:
        allow_auto_checked = ""


# Default home landing page, give environment settings
@app.route('/', methods=['GET'])
def home():
    load_dotenv(override=True)

    r = int(os.getenv("R"))
    g = int(os.getenv("G"))
    b = int(os.getenv("B"))
    allow_auto = os.environ['ALLOW_AUTO']
    allow_auto_checked = os.environ['ALLOW_AUTO_CHECKED']
  

    return render_template('index.html', r=r, g=g, b=b, allow_auto=allow_auto_checked)

# Generic POST for home page
@app.route('/', methods=['POST'])
def setLights():
    
    # There will always be a form id, telling us how to process the form
    form_id = request.form['id']
    from_auto = request.form['auto']
    allow_auto = os.environ['ALLOW_AUTO'] == 'True'
    if allow_auto:
        allow_auto_checked = "checked"
    else:
        allow_auto_checked = ""
    print(os.environ["ALLOW_AUTO"])
    print("bool: {}".format(os.environ['ALLOW_AUTO'] == 'True'))

    # If its from the auto on/off and we dont want it to control anything, ignore it
    if from_auto == 'True' and not allow_auto:
        return render_template('index.html', r=0, g=0, b=0, allow_auto=os.environ['ALLOW_AUTO_CHECKED'])

    if form_id == 'instant':
        # Grab the Color object from the controller class
        color = vals.colors[request.form['color']]

        # Get the rgb value out of the Color object
        r = int(np.uint8(color >> 16))
        g = int(np.uint8(color >> 8))
        b = int(np.uint8(color))

        # Make light changes and update variables
        func.setStrip(strip, color)
        flag='solid'

    elif form_id == 'picker':
        # Process form data
        rgb = request.form['colorpicker']
        colors = rgb[4:-1]
        colors = colors.split(',')

        # Set RGB values from form data
        r = int(colors[0])
        g = int(colors[1])
        b = int(colors[2])

        # Make light changes and update variables
        func.setStrip(strip, Color(r, g, b))
        flag='solid'

    elif form_id == 'special':
        routine = request.form['routine']

        if routine == 'rainbow_fade':
            flag = 'rainbow_fade'
            r = random.randint(0, 255)
            g = random.randint(0, 255)
            b = random.randint(0, 255)
        elif routine == 'turn_off':
            form_id = 'not special'
            r = 0
            g = 0
            b = 0
            func.setStrip(strip, Color(r, g, b))

    elif form_id == 'set_allow_auto':
        print('list: {}'.format(request.form.getlist('allow_auto')))
        if len(request.form.getlist('allow_auto')) == 1:
            #newVal = bool(request.form['allow_auto'].encode('utf-8'))
            newVal = False
            print(newVal)
        else:
            #newVal = bool(request.form.getlist('allow_auto')[1].encode('utf-8'))
            newVal = True
            print(newVal)
        os.environ['ALLOW_AUTO'] = str(newVal)
        s5 = threading.Thread(name='s5', target=setenv, args=('ALLOW_AUTO', newVal))
        s5.start() 
        r = int(os.getenv("R"))
        g = int(os.getenv("G"))
        b = int(os.getenv("B"))
        if newVal:
            os.environ['ALLOW_AUTO_CHECKED'] = "checked"
        else:
            os.environ['ALLOW_AUTO_CHECKED'] = ""

    # Set environment
    # TODO: probably a better way to do 
    if form_id != 'special' and form_id != 'set_allow_auto':
        s2 = threading.Thread(name='s2', target=setenv, args=('R', r))
        s2.start()
        s3 = threading.Thread(name='s3', target=setenv, args=('G', g))
        s3.start()
        s4 = threading.Thread(name='s4', target=setenv, args=('B', b))
        s4.start()


    # Return same page with new variables
    return render_template('index.html', r=r, g=g, b=b, allow_auto=os.environ['ALLOW_AUTO_CHECKED'])


# Run the Flask app
app.run(debug=True, host='0.0.0.0', port=6969)
