#!/usr/bin/env python3
# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

# TODO: implement flags or checks to see if the mode has changed 

import time, os, random, math
from rpi_ws281x import *
from dotenv import load_dotenv

strip1 = range(0, 150)
strip2 = range(150, 300)[::-1]
strip3 = range(300, 450)
strip4 = range(450, 600)[::-1]
strip5 = range(600, 750)
strip6 = range(750, 900)[::-1]
strips = [strip1, strip2, strip3, strip4, strip5, strip6]

colors = {  "WARM_WHITE" : Color(255, 180, 75),
            "COOL_WHITE" : Color(41, 147, 255),
            "WHITE" : Color(255, 255, 255),
            "RED" : Color(255, 0, 0),
            "GREEN" : Color(0, 255, 0),
            "BLUE" : Color(0, 0, 255) }

# LED strip configuration:
LED_COUNT      = 900      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

# Define functions which animate LEDs in various ways.
def setStrip(strip, color, color2):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        if (i % 4) == 0:
            strip.show()
    for i in range(strip.numPixels()):
        strip.setPixelColor(899-i, color2)
        if (i % 4) == 0:
            strip.show()

def colorWipe(strip, color, wait_ms=50, speed=2):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        if (i % (speed*2)) == 0:
            strip.show()

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1, speed=2):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) % 255))
        if (j%speed) == 0:
            strip.show()

def rainbowCycle(strip, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, self.wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()

def allOff(strip):
    print('Turning everything off')
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, 0)
    strip.show()

def allOn(strip, color):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, color)
    strip.show() 

def fadeOn(strip, color, speed=2, max=255):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, color)
    for i in range(0, max, speed):
        strip.setBrightness(i)
        strip.show()
        time.sleep(0.001)

def fadeOff(strip, color, speed=2, max=255):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, color)
    for i in range(max, 0, -speed):
        strip.setBrightness(i)
        strip.show()
        time.sleep(0.001)

def stripOn(strip, color, color2, speed=1):
    for i in range(0, 150):
        for s in strips:
            strip.setPixelColor(s[i], color)
        if (i % speed) == 0:
            strip.show()

    for i in range(0, 150):
        for s in strips:
            strip.setPixelColor(s[149-i], color2)
        if (i % speed) == 0:
            strip.show()


def upOrDown(f, t):
    if f - t < 0:
        return 1
    return -1

def getDelta(f, t, s):
    return float(abs(f - t) / s) * upOrDown(f, t)

def rainbowFade(strip, toR, toG, toB, speed=1):
    steps = 40 - speed
    if steps < 1:
        steps = 1
    # Get current color
    currentColor = strip.getPixelColorRGB(0)
    fromR = currentColor.r
    fromG = currentColor.g
    fromB = currentColor.b
    rDelta = getDelta(fromR, toR, steps)
    gDelta = getDelta(fromG, toG, steps)
    bDelta = getDelta(fromB, toB, steps)

    for j in range(0, steps):
        newR = fromR + rDelta * j
        newG = fromG + gDelta * j
        newB = fromB + bDelta * j
        newColor = Color(int(round(newR)), int(round(newG)), int(round(newB)))
        for i in range(0, strip.numPixels()):
            strip.setPixelColor(i, newColor)

        strip.show()

def stripTest(strip, stripNum):
    c = Color(random.randint(0, 36), random.randint(10, 100), random.randint(60, 220))
    for i in range(0, strip.numPixels()):
        if i in strips[stripNum]:
            strip.setPixelColor(i, c)
    strip.show()

def stripColor(strip, color):
    for i in range(0, strip.numPixels()):
        strip.setPixelColor(i, color)
    strip.show()

# Returns true if val is in between target +/- buffer
def around(val, target, buffer):
    if (val < (target + buffer)) and (val > (target - buffer)):
        return True
    return False

def sineWave(strip, color):
    active = [[], [], [], [], [], []]
    for chunk in active:
        for i in range(0, 150):
            chunk.append(0)
    theta = 0
    buffer = 10
    count = 0
    while True:
        theta += 0.1
        # number between 0 and 156
        sineVal = ((math.sin(theta) + 1) / 2) * 156

        # shift active pixels to the right, starting from the back (covers index 1-155)
        for i in range(149, 0, -1):
            for chunk in active:
                chunk[i] = chunk[i-1]

        for chunk in active:
            chunk[0] = 0

        # set the first pixel in the active list of lists (covers index 0)
        if around(sineVal, 0,   buffer):
            active[0][0] = 1
        if around(sineVal, 50,  buffer):
            active[1][0] = 1
        if around(sineVal, 53,  buffer):
            active[2][0] = 1
        if around(sineVal, 103, buffer):
            active[3][0] = 1
        if around(sineVal, 106, buffer):
            active[4][0] = 1
        if around(sineVal, 156, buffer):
            active[5][0] = 1


        # set pixels according to active list
        for i, ival in enumerate(active):
            for j, jval in enumerate(ival):
                if jval == 1:
                    strip.setPixelColor(strips[i][j], color)
                else:
                    strip.setPixelColor(strips[i][j], Color(0, 0, 0))

        # update strip
        strip.show()

try:
    allOff(strip)



    while True:
        r = random.randint(0, 155)
        g = random.randint(0, 155)
        b = random.randint(0, 155)

        #for i in range(len(strips)):
        #    stripTest(strip, i)
        #    time.sleep(0.75)

        # allOff(strip)
        # time.sleep(.5)


        speed = random.randint(10, 20)
        #speed = 2

        rainbowFade(strip, r, g, b, speed)
        # r2 = random.randint(0, 155)
        # g2 = random.randint(0, 155)
        # b2 = random.randint(0, 155)
        
        #stripOn(strip, Color(r, g, b), Color(r2, g2, b2), speed)
        #setStrip(strip, Color(r, g, b), Color(r2, g2, b2))

        # sineWave(strip, Color(255, 0, 0))


except KeyboardInterrupt:
    allOff(strip)
